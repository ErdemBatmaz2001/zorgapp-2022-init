import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Patient {
    private static final int RETURN = 0;
    private static final int SURNAME = 1;
    private static final int FIRSTNAME = 2;
    private static final int DATEOFBIRTH = 3;
    private static final int HEIGHT = 4;
    private static final int WEIGHT = 5;

    private int id;
    private String surname;
    private String firstName;
    private static LocalDate dateOfBirth;
    private int age;
    private double height;
    private double weight;
    private String bmi;

    static DateTimeFormatter df;
    {

        df = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////////
    Patient(int id, String surname, String firstName, LocalDate dateOfBirth, double height, double weight) {
        this.id = id;
        this.surname = surname;
        this.firstName = firstName;
        this.dateOfBirth = dateOfBirth;
        this.height = height;
        this.weight = weight;
        this.age = setAge();
        this.bmi = setBmi();

    }

    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    public String getSurname() {
        return surname;
    }

    ////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    public String getFirstName() {
        return firstName;
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Calculate patient age
    ////////////////////////////////////////////////////////////////////////////////
    public static int calcAge() {
        LocalDate today = LocalDate.now();
        Period p = Period.between(dateOfBirth, today);
        return p.getYears();
    }

    public int setAge() {
        return this.age = calcAge();
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Calculate patient BMI
    ////////////////////////////////////////////////////////////////////////////////

    private String calcBmi() {
        double bmi = Math.round(weight / (height * height) * 10) / 10.0;
        return String.valueOf(bmi);
    }

    public String setBmi() {
        return this.bmi = calcBmi();
    }

    ///////////////////////////////////////////////////////////////////////////////
    // Display patient data.
    ////////////////////////////////////////////////////////////////////////////////
    public void viewData() {
        System.out.format("===== Patient id=%d ==============================\n", id);
        System.out.format("%-17s %s\n", "Surname:", surname);
        System.out.format("%-17s %s\n", "firstName:", firstName);
        System.out.format("%-17s %s\n", "Date of birth:", dateOfBirth);
        System.out.format("%-17s %s\n", "Age:", age);
        System.out.format("%-17s %s\n", "Height:", height);
        System.out.format("%-17s %s\n", "Weight:", weight);
        System.out.format("%-17s %s\n", "Bmi:", setBmi());

    }


    ////////////////////////////////////////////////////////////////////////////////
    // Shorthand for a Patient's full name
    ////////////////////////////////////////////////////////////////////////////////
    public String fullName() {
        return String.format("%s %s [%s]", firstName, surname, dateOfBirth.toString(), age, height, weight, bmi);
    }

    public void editData() {
        var scanner = new Scanner(System.in);  // User input via this scanner.

        boolean nextCycle = true;
        while (nextCycle) {
            System.out.format("%s\n", "=".repeat(80));
            System.out.format("Current patient: %s\n", fullName());

            ////////////////////////
            // Print menu on screen
            ////////////////////////
            System.out.format("\033[33m%d: \033[0m\n", RETURN);
            System.out.format("%d: Edit surname\n", SURNAME);
            System.out.format("%d: Edit first name\n", FIRSTNAME);
            System.out.format("%d: Edit date of birth\n", DATEOFBIRTH);
            System.out.format("%d: Edit weight\n", WEIGHT);
            System.out.format("%d: Edit height\n", HEIGHT);

            System.out.print("Enter your choice: ");
            int choice = scanner.nextInt();
            switch (choice) {
                case RETURN:
                        // Interrupt the loop
                        nextCycle = false;
                    break;

                case SURNAME:
                    System.out.print("Enter new surname: ");
                    String newSurname = scanner.next();
                    this.surname = newSurname;
                    break;

                case FIRSTNAME:
                    System.out.print("Enter new first name: ");
                    String newFirstName = scanner.next();
                    this.firstName = newFirstName;
                    break;

                case DATEOFBIRTH:
                    System.out.print("Enter new date of birth: ");
                    String newDateOfBirth = scanner.next();
                    this.dateOfBirth = LocalDate.parse(newDateOfBirth);
                    break;

                case WEIGHT:
                    System.out.print("Enter new weight: ");
                    double newWeight = scanner.nextDouble();
                    this.weight = newWeight;
                    break;

                case HEIGHT:
                    System.out.print("Enter new height: ");
                    double newHeight = scanner.nextDouble();
                    this.height = newHeight;
                    break;

                default:
                    System.out.println("Please enter a *valid* digit");
                    break;
            }
        }
    }
}
