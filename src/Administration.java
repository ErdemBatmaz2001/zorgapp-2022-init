import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

///////////////////////////////////////////////////////////////////////////
// class Administration represents the core of the application by showing
// the main menu, from where all other functionality is accessible, either
// directly or via sub-menus.
//
// An Adminiatration instance needs a User as input, which is passed via the
// constructor to the data member 'çurrentUser'.
// The patient data is available via the data member çurrentPatient.
/////////////////////////////////////////////////////////////////
public class Administration
{
   private static final int STOP = 0;
   private static final int VIEW = 1;

   private static final int EDIT = 2;



   private Patient currentPatient;            // The currently selected patient
   private User    currentUser;               // the current user of the program.

   /////////////////////////////////////////////////////////////////
   // Constructor
   /////////////////////////////////////////////////////////////////
   Administration( User user )
   {
      currentUser    = user;
      currentPatient = new Patient( 1, "Van Puffelen", "Pierre", LocalDate.of( 2000, 2, 29 ), 1.8, 87);
      currentPatient = new Patient( 2, "van Baarsen", "Leontien", LocalDate.of( 1939, 1, 20), 1.63, 57.5);

      System.out.format( "Current user: [%d] %s\n", user.getUserID(), user.getUserName() );
   }

   /////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////
   void menu()
   {
      var scanner = new Scanner( System.in );  // User input via this scanner.

      boolean nextCycle = true;
      while (nextCycle)
      {
         System.out.format( "%s\n", "=".repeat( 80 ) );
         System.out.format( "Current patient: %s\n", currentPatient.fullName() );

         ////////////////////////
         // Print menu on screen  aaatest
         ////////////////////////
         System.out.format( "%d:  STOP\n", STOP );
         System.out.format( "%d:  View patient data\n", VIEW );
         System.out.format("%d: Edit patient data\n", EDIT);
         ////////////////////////

         System.out.print( "enter #choice: " );
         int choice = scanner.nextInt();
         switch (choice)
         {
            case STOP: // interrupt the loop
               nextCycle = false;
               break;

            case VIEW:
               currentPatient.viewData();
               break;

            case EDIT:
               // Edit the patient data
               currentUser.editPatientData(currentPatient);
               break;

            default:
               System.out.println( "Please enter a *valid* digit" );
               break;
         }
      }
   }
}
